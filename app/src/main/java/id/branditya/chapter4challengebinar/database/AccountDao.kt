package id.branditya.chapter4challengebinar.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface AccountDao {
    @Query("SELECT * FROM Account")
    fun getAllAccount() : List<Account>

    @Query("SELECT * FROM Account WHERE email = :email AND password = :password")
    fun getRegisteredAccount(email: String?, password: String?) : List<Account>

    @Query("SELECT * FROM Account WHERE email = :email")
    fun getRegisteredAccountEmail(email: String?) : List<Account>

    @Insert(onConflict = REPLACE)
    fun insertAccount(account: Account) : Long

    @Update
    fun updateAccount(account: Account) : Int

    @Delete
    fun deleteAccount(account: Account) : Int
}