package id.branditya.chapter4challengebinar.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface NoteDao {
    @Query("SELECT * FROM Note WHERE student_id = :studentId")
    fun getAllNote(studentId : Int) : List<Note>

    @Insert(onConflict = REPLACE)
    fun insertNote(note: Note) : Long

    @Update
    fun updateNote(note: Note) : Int

    @Delete
    fun deleteNote(note: Note) : Int
}