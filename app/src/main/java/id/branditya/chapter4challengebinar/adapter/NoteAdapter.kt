package id.branditya.chapter4challengebinar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import id.branditya.chapter4challengebinar.R
import id.branditya.chapter4challengebinar.database.Note

class NoteAdapter(private val listener: NoteActionListener) : RecyclerView.Adapter<NoteAdapter.NoteViewHolder>(){
    private val diffCallback = object : DiffUtil.ItemCallback<Note>() {
        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }

        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this,diffCallback)

    fun updateData(notes: List<Note>) = differ.submitList(notes)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note,parent,false)
        return NoteViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class NoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvNoteTitle = view.findViewById<TextView>(R.id.tv_note_title)
        private val tvNoteContent = view.findViewById<TextView>(R.id.tv_note_content)
        private val btnEdit = view.findViewById<ImageView>(R.id.btn_edit)
        private val btnDelete = view.findViewById<ImageView>(R.id.btn_delete)

        fun bind(note: Note) {
            tvNoteTitle.text = note.title
            tvNoteContent.text = note.content

            btnEdit.setOnClickListener {
                listener.onEdit(note)
            }

            btnDelete.setOnClickListener {
                listener.onDelete(note)
            }
        }
    }
}

interface NoteActionListener {
    fun onDelete(note: Note)
    fun onEdit(note: Note)
}