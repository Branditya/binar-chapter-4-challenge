package id.branditya.chapter4challengebinar.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Account(
    @PrimaryKey(autoGenerate = true) val id : Int?,
    @ColumnInfo(name = "username") val username : String,
    @ColumnInfo(name = "email") val email : String,
    @ColumnInfo(name = "password") val password : String
)
