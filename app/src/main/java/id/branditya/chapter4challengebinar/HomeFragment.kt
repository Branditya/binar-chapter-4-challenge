package id.branditya.chapter4challengebinar

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.branditya.chapter4challengebinar.adapter.NoteActionListener
import id.branditya.chapter4challengebinar.adapter.NoteAdapter
import id.branditya.chapter4challengebinar.database.AccountDatabase
import id.branditya.chapter4challengebinar.database.Note
import id.branditya.chapter4challengebinar.databinding.FragmentHomeBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {
    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var noteAdapter : NoteAdapter
    private var mDb : AccountDatabase? = null
    private var accountId : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = requireContext().getSharedPreferences("login", Context.MODE_PRIVATE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = AccountDatabase.getInstance(requireContext())
        accountId = sharedPreferences.getInt("accountId", 0)
        changeTvTitle()
        logoutBtnClicked()
        showEmptyNoteStatus()
        initRecyclerView()
        getDataFromDb()
        addFabClicked()
    }


    @SuppressLint("SetTextI18n")
    private fun changeTvTitle() {
        val username = sharedPreferences.getString("username","default").toString()
        binding.tvTitle.text = "Welcome, $username!"
    }

    private fun logoutBtnClicked() {
        binding.btnLogout.setOnClickListener {
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
            findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
        }
    }

    private fun showEmptyNoteStatus() {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.getAllNote(accountId)
            if (!result.isNullOrEmpty()) {
                CoroutineScope(Dispatchers.Main).launch {
                    binding.tvEmptyStatus.visibility = View.GONE
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    binding.tvEmptyStatus.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun initRecyclerView() {
        binding.apply {
            noteAdapter = NoteAdapter(action)
            rvNote.adapter = noteAdapter
            rvNote.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun addFabClicked() {
        binding.fabAdd.setOnClickListener {
            showAddAlertDialog(null)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showAddAlertDialog(note: Note?) {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialog_add, null, false)

        val tvTitle = customLayout.findViewById<TextView>(R.id.tv_title)
        val etTitle = customLayout.findViewById<EditText>(R.id.et_note_title)
        val etContent = customLayout.findViewById<EditText>(R.id.et_note_content)
        val btnInput = customLayout.findViewById<Button>(R.id.btn_input_note)
        val btnCancel = customLayout.findViewById<Button>(R.id.btn_cancel_input)

        if (note!= null) {
            tvTitle.text = "Edit Data"
            btnInput.text = "Update"
            etTitle.setText(note.title)
            etContent.setText(note.content)
        }
        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)
        val dialog = builder.create()
        dialog.setCancelable(false)
        btnInput.setOnClickListener {
            var title = etTitle.text.toString()
            val content = etContent.text.toString()
            if (title.isEmpty()) {
                title = "Untitled"
            }
            if (note!= null) {
                val updateNote = Note(note.id, accountId, title, content)
                updateToDb(updateNote)
            } else {
                saveToDb(title, content)
            }
            dialog.dismiss()
        }
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun updateToDb(note: Note) {
        val newNote = Note(note.id, accountId, note.title, note.content)
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.updateNote(newNote)
            if (result!= 0) {
                getDataFromDb()
                showToastInMainThread("Data Berhasil Diupdate")
            } else {
                showToastInMainThread("Data Gagal Diupdate")
            }
        }
    }

    private fun saveToDb(title: String, content: String) {
        val note = Note(null, accountId, title, content)
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.insertNote(note)
            if (result != 0L) {
                getDataFromDb()
                showToastInMainThread("Data Berhasil Ditambah")
            } else {
                showToastInMainThread("Data Gagal Ditambah")
            }
        }
    }

    private fun getDataFromDb() {
        showEmptyNoteStatus()
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.getAllNote(accountId)
            if (result!= null) {
                CoroutineScope(Dispatchers.Main).launch {
                    noteAdapter.updateData(result)
                }
            }
        }
    }

    private val action = object : NoteActionListener {
        override fun onDelete(note: Note) {
            showDeleteAlertDialog(note)
        }

        override fun onEdit(note: Note) {
            showAddAlertDialog(note)
        }
    }

    private fun showDeleteAlertDialog(note: Note) {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialog_delete, null, false)

        val btnDelete = customLayout.findViewById<Button>(R.id.btn_delete_note)
        val btnCancel = customLayout.findViewById<Button>(R.id.btn_cancel_delete)

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)
        val dialog = builder.create()
        dialog.setCancelable(false)
        btnDelete.setOnClickListener {
            deleteDataFromDb(note)
            dialog.dismiss()
        }
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun deleteDataFromDb(note: Note) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.deleteNote(note)
            if (result != 0) {
                getDataFromDb()
                showToastInMainThread("Data Berhasil Dihapus")
            } else {
                showToastInMainThread("Data Gagal Dihapus")
            }
        }
    }

    private fun showToastInMainThread(message: String) {
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(requireContext(),message,Toast.LENGTH_SHORT).show()
        }
    }
}