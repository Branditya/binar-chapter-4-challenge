package id.branditya.chapter4challengebinar.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Note(
    @PrimaryKey(autoGenerate = true) val id : Int?,
    @ColumnInfo(name = "student_id") val studentId : Int,
    @ColumnInfo(name = "title") val title : String,
    @ColumnInfo(name = "Content") val content : String
)
